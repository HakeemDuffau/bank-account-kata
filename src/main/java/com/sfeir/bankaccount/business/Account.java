package com.sfeir.bankaccount.business;

import com.sfeir.bankaccount.infrastructure.StatementsPrinter;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Logger;

import static com.sfeir.bankaccount.business.OperationType.DEPOSIT;
import static com.sfeir.bankaccount.business.OperationType.WITHDRAWAL;
import static java.util.logging.Level.WARNING;

public class Account {
    private UUID id;
    private Statements statements;
    private Clock clock;

    private Account(Builder builder) {
        setId(builder.id);
        setStatement(builder.statements);
        setClock(builder.clock);
    }

    public void deposit(Amount amount) {
        var balance = DEPOSIT.execute(statements.getBalance(), amount);
        statements.save(LocalDateTime.now(clock), DEPOSIT, amount, balance);
    }

    public void withdrawal(Amount amount) {
        try {
            var balance = WITHDRAWAL.execute(statements.getBalance(), amount);
            statements.save(LocalDateTime.now(clock), WITHDRAWAL, amount, balance);
        } catch (InsufficientFundsException insufficientFundsException) {
            Logger.getAnonymousLogger().log(WARNING, insufficientFundsException.getMessage());
        }
    }

    public void showHistory(StatementsPrinter statementsPrinter) {
        statements.print(statementsPrinter);
    }

    void setId(UUID id) {
        this.id = id;
    }

    void setStatement(Statements statements) {
        this.statements = statements;
    }

    void setClock(Clock clock) {
        this.clock = clock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id.equals(account.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public static final class Builder {
        private final UUID id;
        private Statements statements = new Statements(new LinkedList<>());
        private Clock clock = Clock.systemDefaultZone();

        private Builder(UUID id) {
            this.id = id;
        }

        public static Builder buildAccount(UUID id) {
            return new Builder(id);
        }

        public Builder withStatements(Statements val) {
            statements = val;
            return this;
        }

        public Builder withClock(Clock val) {
            clock = val;
            return this;
        }

        public Account build() {
            return new Account(this);
        }
    }
}
