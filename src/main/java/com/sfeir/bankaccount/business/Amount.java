package com.sfeir.bankaccount.business;

import java.math.BigDecimal;
import java.util.Objects;

public record Amount(BigDecimal value) {

    static final String NEGATIVE_VALUE_ERROR_MSG = "An amount cannot have a negative value";
    static final String NULL_VALUE_ERROR_MSG = "An amount cannot have a null value";

    public Amount {
        if (value == null)
            throw new IllegalArgumentException(NULL_VALUE_ERROR_MSG);
        if (value.compareTo(BigDecimal.ZERO) < 0)
            throw new IllegalArgumentException(NEGATIVE_VALUE_ERROR_MSG);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Amount amount = (Amount) o;
        return Objects.equals(value, amount.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

}
