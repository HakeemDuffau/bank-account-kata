package com.sfeir.bankaccount.business;

public interface Operation {
    Amount execute(Amount balance, Amount amount);
}
