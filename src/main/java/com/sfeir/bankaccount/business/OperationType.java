package com.sfeir.bankaccount.business;

public enum OperationType implements Operation {

    DEPOSIT((balance, amount) -> new Amount(balance.value().add(amount.value()))),
    WITHDRAWAL(((balance, amount) -> {
        if (balance.value().compareTo(amount.value()) < 0)
            throw new InsufficientFundsException(OperationType.INSUFFICIENT_FOUNDS_ERROR_MSG);
        return new Amount(balance.value().subtract(amount.value()));
    }));

    private final Operation operation;

    static final String INSUFFICIENT_FOUNDS_ERROR_MSG = "There are not enough founds to complete this transaction";


    OperationType(Operation operation) {
        this.operation = operation;
    }

    @Override
    public Amount execute(Amount balance, Amount amount) {
        return operation.execute(balance, amount);
    }
}
