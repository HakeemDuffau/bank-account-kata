package com.sfeir.bankaccount.business;

import java.time.LocalDateTime;
import java.util.Objects;

public record Statement(LocalDateTime dateTime, OperationType operationType, Amount amount, Amount balance) {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Statement statement = (Statement) o;
        return Objects.equals(dateTime, statement.dateTime) && operationType == statement.operationType && Objects.equals(amount, statement.amount) && Objects.equals(balance, statement.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTime, operationType, amount, balance);
    }
}
