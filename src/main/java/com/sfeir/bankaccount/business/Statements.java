package com.sfeir.bankaccount.business;

import com.sfeir.bankaccount.infrastructure.StatementsPrinter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Statements {
    private final LinkedList<Statement> lines;

    public Statements(List<Statement> lines) {
        this.lines = new LinkedList<>(lines);
    }

    public List<Statement> getLines() {
        return lines;
    }

    void save(LocalDateTime dateTime, OperationType operationType, Amount amount, Amount balance) {
        lines.add(new Statement(dateTime, operationType, amount, balance));
    }

    void print(StatementsPrinter statementsPrinter) {
        statementsPrinter.print(this);
    }

    Amount getBalance() {
        return lines.isEmpty() ? new Amount(BigDecimal.ZERO) : lines.getLast().balance();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Statements that = (Statements) o;
        return Objects.equals(lines, that.lines);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lines);
    }
}
