package com.sfeir.bankaccount.infrastructure;

import com.sfeir.bankaccount.business.Statements;

public interface StatementsPrinter {
    void print(Statements statements);
}
