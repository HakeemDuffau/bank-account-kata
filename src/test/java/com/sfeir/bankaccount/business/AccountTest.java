package com.sfeir.bankaccount.business;

import com.sfeir.bankaccount.infrastructure.StatementsPrinter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.UUID;

import static com.sfeir.bankaccount.business.OperationType.DEPOSIT;
import static com.sfeir.bankaccount.business.OperationType.WITHDRAWAL;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountTest {
    @Mock
    private Statements statementsMock;
    private final Amount initialBalance = new Amount(BigDecimal.valueOf(9));
    private final Clock fixedClock = Clock.fixed(Instant.now(), ZoneId.systemDefault());

    private Account account;

    @BeforeEach
    void setUp() {
        var accountId = UUID.randomUUID();

        this.account = Account.Builder.buildAccount(accountId)
                .withClock(fixedClock)
                .withStatements(statementsMock)
                .build();
    }

    @Test
    void should_execute_deposit_operation_and_save_statement_when_making_deposit() {
        mockInitialBalance();
        var amountToBeDeposited = new Amount(BigDecimal.TEN);
        var expectedBalanceAfterDeposit = DEPOSIT.execute(initialBalance, amountToBeDeposited);
        Mockito.doNothing().when(statementsMock).save(LocalDateTime.now(fixedClock), DEPOSIT, amountToBeDeposited, expectedBalanceAfterDeposit);

        account.deposit(amountToBeDeposited);

        verify(statementsMock, times(1)).getBalance();
        verify(statementsMock, times(1)).save(LocalDateTime.now(fixedClock), DEPOSIT, amountToBeDeposited, expectedBalanceAfterDeposit);
    }


    @Test
    void should_execute_withdrawal_operation_and_save_statement_when_making_withdrawal() {
        mockInitialBalance();
        var amountToBeWithdrawn = new Amount(BigDecimal.ONE);
        var expectedBalanceAfterWithdrawal = WITHDRAWAL.execute(initialBalance, amountToBeWithdrawn);
        Mockito.doNothing().when(statementsMock).save(LocalDateTime.now(fixedClock), OperationType.WITHDRAWAL, amountToBeWithdrawn, expectedBalanceAfterWithdrawal);

        account.withdrawal(amountToBeWithdrawn);

        verify(statementsMock, times(1)).getBalance();
        verify(statementsMock, times(1)).save(LocalDateTime.now(fixedClock), OperationType.WITHDRAWAL, amountToBeWithdrawn, expectedBalanceAfterWithdrawal);
    }

    @Test
    void should_not_save_any_statement_when_the_mount_to_be_withdrawn_is_greater_than_the_balance() {
        mockInitialBalance();
        var amountToBeWithdrawn = new Amount(BigDecimal.valueOf(Long.MAX_VALUE));

        account.withdrawal(amountToBeWithdrawn);

        verify(statementsMock, times(1)).getBalance();
        verify(statementsMock, never()).save(any(LocalDateTime.class), any(OperationType.class), any(Amount.class), any(Amount.class));
    }

    @Test
    void should_print_statement_when_showing_history() {
        var statementPrinterMock = mock(StatementsPrinter.class);
        doNothing().when(statementsMock).print(statementPrinterMock);

        account.showHistory(statementPrinterMock);
        verify(statementsMock, times(1)).print(statementPrinterMock);
    }

    private void mockInitialBalance() {
        when(statementsMock.getBalance()).thenReturn(initialBalance);
    }

}