package com.sfeir.bankaccount.business;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

class AmountTest {

    private static Stream<Arguments> provideInvalidAmountValue() {
        return Stream.of(
                Arguments.of(null, Amount.NULL_VALUE_ERROR_MSG),
                Arguments.of(BigDecimal.TEN.negate(), Amount.NEGATIVE_VALUE_ERROR_MSG)
        );
    }

    @ParameterizedTest
    @MethodSource("provideInvalidAmountValue")
    void should_throw_illegal_arg_exception_when_amount_is_initialized_with_a_invalid_value(BigDecimal value, String exceptionMsg) {

        assertThrowsExactly(IllegalArgumentException.class,
                () -> new Amount(value),
                exceptionMsg);
    }

}
