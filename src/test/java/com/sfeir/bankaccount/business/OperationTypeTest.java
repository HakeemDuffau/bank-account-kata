package com.sfeir.bankaccount.business;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

class OperationTypeTest {

    private final Amount balance = new Amount(BigDecimal.valueOf(9));

    @Test
    void should_return_new_balance_equals_to_the_balance_amount_plus_the_deposit_amount_when_execute_deposit() {
        var amountToBeDeposited = new Amount(BigDecimal.TEN);
        var expectedBalanceAfterDeposit = new Amount(BigDecimal.valueOf(19));

        var result = OperationType.DEPOSIT.execute(balance, amountToBeDeposited);

        assertEquals(result, expectedBalanceAfterDeposit);
    }

    @Test
    void should_return_new_balance_equals_to_the_balance_amount_minus_the_withdrawal_amount_when_execute_deposit() {
        var amountToBeWithdrawn = new Amount(BigDecimal.ONE);
        var expectedBalanceAfterWithdrawal = new Amount(BigDecimal.valueOf(8));

        var result = OperationType.WITHDRAWAL.execute(balance, amountToBeWithdrawn);

        assertEquals(result, expectedBalanceAfterWithdrawal);
    }

    @Test
    void should_throw_insufficient_funds_exception_when_the_withdrawal_amount_is_greater_than_balance() {
        var amountToBeWithdrawn = new Amount(BigDecimal.TEN);

        assertThrowsExactly(InsufficientFundsException.class,
                () -> OperationType.WITHDRAWAL.execute(balance, amountToBeWithdrawn),
                OperationType.INSUFFICIENT_FOUNDS_ERROR_MSG);

    }
}