package com.sfeir.bankaccount.business;

import com.sfeir.bankaccount.infrastructure.StatementsPrinter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;

class StatementsTest {

    private Statements statements;

    @BeforeEach
    void setUp() {
        this.statements = new Statements(new LinkedList<>());
    }

    @Test
    void should_add_a_new_statement_to_the_list() {
        var dateTime = LocalDateTime.now();
        var operationType = OperationType.DEPOSIT;
        var amount = new Amount(BigDecimal.TEN);
        var balance = new Amount(BigDecimal.TEN);
        var expectedStatements = Collections.singletonList(buildStatement(dateTime, operationType, amount, balance));

        statements.save(dateTime, operationType, amount, balance);

        Assertions.assertEquals(expectedStatements, statements.getLines());
    }

    @Test
    void should_call_statement_printer_when_printing_statement() {
        var statementPrinterMock = mock(StatementsPrinter.class);
        doNothing().when(statementPrinterMock).print(this.statements);

        statements.print(statementPrinterMock);

        verify(statementPrinterMock, times(1)).print(this.statements);
    }

    @Test
    void should_return_latest_statement_balance_when_getting_current_balance() {
        var firstStatement = buildStatement(LocalDateTime.now(), OperationType.DEPOSIT, new Amount(BigDecimal.TEN), new Amount(BigDecimal.TEN));
        var lastStatement = buildStatement(LocalDateTime.now(), OperationType.WITHDRAWAL, new Amount(BigDecimal.TEN), new Amount(BigDecimal.ZERO));
        statements = new Statements(List.of(firstStatement, lastStatement));

        var result = statements.getBalance();

        Assertions.assertEquals(result, lastStatement.balance());
    }

    @Test
    void should_return_balance_with_amount_equals_zero_when_getting_current_balance_before_any_statement_have_been_save() {
        var expectedCurrentBalance = new Amount(BigDecimal.ZERO);
        var result = statements.getBalance();

        Assertions.assertEquals(result, expectedCurrentBalance);
    }

    private Statement buildStatement(LocalDateTime dateTime, OperationType operationType, Amount amount, Amount balance) {
        return new Statement(dateTime, operationType, amount, balance);
    }

}
