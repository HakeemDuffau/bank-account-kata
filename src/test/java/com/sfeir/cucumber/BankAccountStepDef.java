package com.sfeir.cucumber;

import com.sfeir.bankaccount.business.*;
import com.sfeir.bankaccount.infrastructure.StatementsPrinter;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BankAccountStepDef {
    private Account mAccount;

    private Statements statements;
    private final Clock fixedClock = Clock.fixed(Instant.parse("2023-07-20T10:15:30.00Z"), ZoneId.systemDefault());

    private final StatementsPrinter statementsPrinterMock = Mockito.mock(StatementsPrinter.class);

    @DataTableType
    public Statement mapToStatement(Map<String, String> entry) {
        var dateTime = LocalDateTime.parse(entry.get("dateTime"));
        var operationType = OperationType.valueOf(entry.get("operationType"));
        var amount = new Amount(new BigDecimal(entry.get("amount")));
        var balance = new Amount(new BigDecimal(entry.get("balance")));

        return new Statement(dateTime, operationType, amount, balance);
    }

    @Given("my bank account with a balance of {bigdecimal}")

    public void myBankAccountOfBalance(BigDecimal balance) {
        statements = new Statements(List.of(
                buildFirstStatement(new Amount(balance), new Amount(balance))
        ));

        this.mAccount = Account.Builder.buildAccount(UUID.randomUUID())
                .withStatements(statements)
                .withClock(fixedClock)
                .build();
    }

    @When("I make a {word} of {bigdecimal}")
    public void iMakeAOperationOfAmount(String operation, BigDecimal amount) {
        switch (operation) {
            case "deposit" -> this.mAccount.deposit(new Amount(amount));
            case "withdrawal" -> this.mAccount.withdrawal(new Amount(amount));
            default -> throw new IllegalArgumentException();
        }
    }

    @When("I show the history")
    public void iShowTheHistory() {
        this.mAccount.showHistory(statementsPrinterMock);
    }

    @Then("my account statements are :")
    public void myAccountStatementsAre(List<Statement> lines) {
        var expectedStatements = new Statements(lines);
        this.mAccount.showHistory((statements) -> assertEquals(statements, expectedStatements));
    }

    @Then("my account statements are printed")
    public void myAccountStatementIsShown() {
        Mockito.verify(statementsPrinterMock, Mockito.times(1)).print(statements);
    }

    private Statement buildFirstStatement(Amount amount, Amount balance) {
        return new Statement(LocalDateTime.now(fixedClock), OperationType.DEPOSIT, amount, balance);
    }


}
