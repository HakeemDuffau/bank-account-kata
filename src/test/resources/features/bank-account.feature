Feature: Bank account operations

  Background:
    Given my bank account with a balance of 20.0

  Scenario: Deposit money in my bank account
    When I make a deposit of 15.0
    Then my account statements are :
      | dateTime            | operationType | amount | balance |
      | 2023-07-20T12:15:30 | DEPOSIT       | 20.0   | 20.0    |
      | 2023-07-20T12:15:30 | DEPOSIT       | 15.0   | 35.0    |

  Scenario: Withdrawal money from my bank account
    When I make a withdrawal of 5.0
    Then my account statements are :
      | dateTime            | operationType | amount | balance |
      | 2023-07-20T12:15:30 | DEPOSIT       | 20.0   | 20.0    |
      | 2023-07-20T12:15:30 | WITHDRAWAL    | 5.0    | 15.0    |

  Scenario: Show history of my bank account
    When I show the history
    Then my account statements are printed